from mockupio import MockupClient


def process_image(img, **kwargs):
    if kwargs is not None:
        pass


camera_options = {
    "index_device": 0, # Index camera device 
    "size": (320, 240), # Image size
    "quality": 90, # jpeg quality
    "fps_record": 10
}

serial_options = {
    "port": None, 
    "baudrate": 9600,
    "timeout": 0.25,
    "reconnection_time_serial": 1
}

socketio_options = {
    "server_address": "http://localhost:3000",
    "identifier": "MAQUETA-MCU",
    "fps_stream": 8,
    "reconnection_time_socketioio": 1,
    "auto_stream": True,
    "stream_to_host": False
}

extra_options = {
    "multiprocess_camera": True,
    "multiprocess_serial": False,
    "multiprocess_socketio": False,
    "debug_camera": True,
    "debug_serial": True,
    "debug_socketio": True
}


class Mockup:
    def __init__(self, 
            camera_options, 
            serial_options, 
            socketio_options, 
            extra_options,
            **kwargs):
        super().__init__()
        self.variables = {}
        # --------------------------------------Initialize Client ---------------------------------

        self.client = MockupClient(**camera_options, **serial_options, **socketio_options, **extra_options)

        # ----------------------------------- Mockup Callbacks ------------------------------------
        self.client.on('camera-image-ready', self.show_image)
        self.client.on('camera-record-end', self.record_end)

        self.client.on('serial-connection-event', self.handle_serial_connection_status)
        self.client.on('serial-incoming-data', self.recive_data_serial)

        self.client.on('socketio-connection-event', self.handle_socketio_connection_status)
        self.client.on('socketio-request-updates', self.response_to_server)
        self.client.on('socketio-incoming-data', self.recive_data_socketio)
        self.client.on('socketio-stop-event', self.handle_stop_event_socketio)

        self.client.on('error', self.handle_errors)
        
        # Multiprocessing Functions ----------------------------------------------------------------

        # self.params = self.client.obtain_dict()
        # self.params["color"] = 1
        # self.client.camera.set_callback('after-read', process_image, **self.params)
        # self.client.camera.set_callback('after-record', export_data, **params)
        # -------------------------------------------------------------------------------------------

        self.client.start(camera=True, serial=True, socketio=True)
        # GUI events --------------------------------------------------------------------------------
        # self.btn.clicked.connect(lambda value: self.gui_event(id='btn', value=value))

    def gui_events(self, id, value, send=True,toSerial=True, toServer=True):
        self.client.backup(self.variables)
        self.variables[id] = value
        if send:
            self.client.write(self.variables, toSerial=toSerial, toServer=toServer)

    def set_gui_values(self, data):
        for _id in data:
            if hasattr(self, _id):
                attribute = getattr(self, _id)
                value = data[_id]
                # Parse values if you need it
                if isinstance(value, str):
                    pass 
                if isinstance(value, int):
                    pass 
                if isinstance(value, float):
                    pass
                if isinstance(value, bool):
                    pass

    def show_image(self,frame, results):
        print("Showing frame...")
    
    def record_end(self):
        print("Recording camera ended!")

    def handle_serial_connection_status(self, status):
        print("Serial connection status: ", status) 

    def recive_data_serial(data):
        print("Serial says: ", data)        

    def handle_stop_event_socketio(self):
        print("stoping mockup")

    def handle_socketio_connection_status(self, status):
        print("socketio connection status: ", status) 
    
    def response_to_server(self):
        print("Responding to server ...")
    
    def recive_data_socketio(self, data):
        print("Socketio recives: ", data)
    
    def handle_errors(self, error):
        print(error)


m = Mockup(camera_options, serial_options, socketio_options, extra_options)