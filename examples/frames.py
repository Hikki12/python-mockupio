from mockupio import MockupClient

extra_options = {
    "multiprocess_camera": True,
    "multiprocess_serial": False,
    "multiprocess_socketio": False,
    "debug_camera": True,
    "debug_serial": True,
    "debug_socketio": True
}

# def image_readed(img):
#     global image
#     image = img

# server_address="http://localhost:3000"
# identifier = "MAQUETA-MCU"

# def algo(data):
#     print("Algo : ", data)

# client = MockupClient(server_address=server_address, identifier=identifier, fps_record=15, fps_stream=12)
# #client.on('camera-image-ready', image_readed)

# client.on('serial-incoming-data', algo)
# client.start_camera()
# client.start_socketio()
# client.start_serial()

# # Camera
# image = None
# delay = 1/10

# while True:
#     if image is not None:
#         cv2.imshow("image", image)
#         if cv2.waitKey(1) & 0xFF == ord('q'):
#             break
#         time.sleep(delay)
# cv2.destroyAllWindows()
# client.stop()