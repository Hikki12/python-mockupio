#include <ArduinoJson.h>
#include <SimpleTimer.h>

/*
Events:
  Recived:
    $stop: it will stop experiment
    $status: it asks for the current state
  Emitted: 
    $recived -> it notifies the data were recived
    $status, ok -> response status of the system is ok
    $status, error:msg  -> response system has some error
*/

/*------------------------------------- PINS AREA -------------------------------------------*/
const int pin1 = 4;
const int pin2 = 5;
const int pin3 = 6;

/*-------------------------------- VARIABLES FOR SERIAL -------------------------------------*/

String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete

/*--------------------------------- VARIABLES ÁREA -------------------------------------------*/
// Initialize json object
StaticJsonDocument<512> doc;
//
bool var1 = false;
int var2 = 100;
float var3 = 3.211;
long var4 = 21111;

/*------------------------------------SEND/READ INFO-----------------------------------------*/
void sendVariables(){
  doc["var1"] = var1;
  doc["var2"] = var2;
  doc["var3"] = var3;
  doc["var4"] = var4;
  serializeJson(doc, Serial);  
  Serial.println();
}

void dataRecived(){
  Serial.println("$recived");
}

void readVariables(String data){
  
  DeserializationError error = deserializeJson(doc, data);
  
  if(error){
    // Serial.println(error.f_str());
    return;
  }
  // Set Variables
  var1 = doc["var1"];
  var2 = doc["var2"];
  var3 = doc["var3"];
  var4 = doc["var4"];
  // Notify the data were recived
  dataRecived();
}

void serialEvent() {
  while (Serial.available()) {
    char inChar = (char) Serial.read();
    inputString += inChar;
    
    if (inChar == '\n') {
      
      if(inputString[0] == '$'){
        readEvents(inputString);
      }else{
        readVariables(inputString);
      }
      
      stringComplete = true;
      inputString = "";
    }
  }
}

void readEvents(String data){
    String event = data.substring(1);
    event.trim();
    if(event == "stop"){
      stop();
    }
    if(event == "status"){
      areYouOk();
    }
}

/*----------------------------------------- EVENTS ----------------------------------------------*/
SimpleTimer stopTimer(60000);
int maxTime = 1; // Minutes
int minutes = 0; 

void autoStop(){
  if(stopTimer.isReady()){
    minutes++;
    stopTimer.reset();
  }
  if(minutes >= maxTime){
    stop();
    minutes = 0;
  }
}

/*------------------------------------- LOGIC FUNCTIONS -----------------------------------------*/
void stop(){
   Serial.println("$stopped");
}

void lightControl(){
  // digitalWrite(light, lightIsOn);
}

void motorControl(){
  //
}

void sensorReading(){
  //
}

void areYouOk(){
  bool ok = true;
  String error = "";
  
  // check if are there any error on the system
  if(ok){
    Serial.println("$status,ok");
  }else{
    Serial.println("$status,error: " + error);
  }
}

/*------------------------------------------ SET UP -----------------------------------------------*/
void configurePins(){
  pinMode(OUTPUT, pin1);
  pinMode(OUTPUT, pin2);
  pinMode(OUTPUT, pin3);
}

void setup() {
  Serial.begin(9600);
  inputString.reserve(512);
  configurePins();
}

void loop() {
  autoStop();
  lightControl();
}