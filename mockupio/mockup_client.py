from threading import Thread, Event
from multiprocessing import Manager
import time 
from .events import Event as EventEmitter
from .camera import Camera
from .custom_serial import Serial
from .socketio_async_client import SocketIO
from .threads_utils import enumerate_threads


class MockupClient:
    """A Mockup client.
    This class allows to you to manage socketio communications, the camera and serial communications.
    It could be used in a threads only mode, processes only mode, or hibrid mode which seperate cpu 
    proccesing between the cpu cores. 
    """
    def __init__(
            self,
            # Options for socketio 
            server_address="", 
            identifier="",
            reconnection_time_socketio=1,
            fps_stream=8, 
            auto_stream=True,
            stream_to_host=False,
            max_stream_time=1800,
            write_delay=0.25,
            latency=0.005,
            # Options for camera
            index_device=0,
            size=(320, 240),
            fps_record=10,
            quality=80,
            h_flip=True,
            v_flip=False,
            dt_fix=0.01,
            rotate=0,
            power_save=True,
            # Options for serial
            port=None,
            baudrate=9600,
            timeout=0.25,
            reconnection_time_serial=2,
            # Extra options
            multiprocess_camera=True,
            multiprocess_serial=True,
            multiprocess_socketio=True,
            debug_camera=True,
            debug_serial=True,
            debug_socketio=True,
            engine_logger=True,
            socketio_logger=True,
            check_delay=0.25,
            **kwargs):

        self.check_delay = check_delay
        # Define multiprocess manager
        self.manager = None
        
        if multiprocess_camera or multiprocess_serial or multiprocess_socketio:
            self.manager = Manager()

        self.camera = Camera(manager=self.manager, i=index_device, size=size, fps=fps_record, quality=quality, 
                             multiprocess=multiprocess_camera, debug=debug_camera, h_flip=h_flip, v_flip=v_flip,
                             dt_fix=dt_fix, power_save=power_save,rotate=rotate)

        self.socketio = SocketIO(manager=self.manager, server_address=server_address, identifier=identifier, 
                                 fps=fps_stream, reconnection_delay=reconnection_time_socketio, 
                                 multiprocess=multiprocess_socketio, debug=debug_socketio, auto_stream=auto_stream,
                                 stream_to_host=stream_to_host, max_stream_time=max_stream_time, write_delay=write_delay,
                                 latency=latency, engine_logger=engine_logger, socketio_logger=socketio_logger)

        self.serial = Serial(manager=self.manager, port=port, baudrate=baudrate, timeout=timeout,
                             reconnection_delay=reconnection_time_serial, multiprocess=multiprocess_serial,
                             debug=debug_serial)
        
        self.events = EventEmitter()
        # Events
        self.running = Event()
        self.thread = Thread(target=self.run, name="Events-Thread")
        # Backup
        self._backup = None

    def on(self, event:str, callback):
        """Set and event and a callback function"""
        self.events.on(event, callback)

    def check_camera_events(self):
        if self.camera.wait_event.is_set():
            data = self.camera.read()
            image, image64, results = data
            self.events.emit('camera-image-ready', image, results)
            self.socketio.set_new_frame(image64)
        else:
            time.sleep(self.check_delay)

    def check_socketio_events(self):
        """It checks socketio events"""
        if self.socketio.connection_event.is_set():
            self.events.emit('socketio-connection-event', self.socketio.is_connected())
            self.socketio.connection_event.clear()
        
        if self.socketio.stop_event.is_set():
            self.events.emit('socketio-stop-event')
            self.socketio.stop_event.clear()
        
        if self.socketio.incoming_data.is_set():
            self.events.emit('socketio-incoming-data', self.socketio.get_data())
            self.socketio.incoming_data.clear()
        
        if self.socketio.busy_event.is_set():
            self.events.emit('socketio-busy', self.socketio.is_busy())
            if self.camera.power_save:
                if self.socketio.is_busy():
                    self.camera.active_cpu()
                else:
                    self.camera.relax_cpu()
            self.socketio.busy_event.clear()

    def check_serial_events(self):
        """It checks serial events"""
        if self.serial.connection_event.is_set():
            self.events.emit('serial-connection-event', self.serial.is_connected())
            self.serial.connection_event.clear()

        if self.serial.incoming_data.is_set():
            self.events.emit('serial-incoming-data', self.serial.get_data())
            self.serial.incoming_data.clear()

    def run(self):
        """Main loop for check events coming from socketio, camera and serial processes"""
        while self.running.is_set():
            if self.camera.running.is_set():
                self.check_camera_events()

            if self.socketio.running.is_set():
                self.check_socketio_events()

            if self.serial.running.is_set():
                self.check_serial_events()

    def write_to_serial(self, data, toJson=True):
        """It writes data to a serial device"""
        self.serial.write(data, toJson=toJson)
    
    def write_to_server(self, data):
        """It writes data to a socketio server"""
        self.socketio.write(data)

    def write(self, data, toSerial=True, toServer=True, toJson=True):
        """It allows to write data with socketio and serial communications
        :param data: data to send
        :param toSerial: ``True`` to write with serial protocol
        :param toSocketio: ``True`` to write with socketio protocol
        """
        if toSerial:
            self.write_to_serial(data, toJson=toJson)
        if toServer:
            self.write_to_server(data)

    def start(self, camera:bool=True, serial:bool=True, socketio:bool=True):
        """It starts threads or processes for manage events"""
        self.running.set()
        if camera:
            self.camera.start()
        if serial:
            self.serial.start()
        if socketio:
            self.socketio.start()
        self.thread.start()
        enumerate_threads(is_main=True)

    def stop(self):
        """It stops the main loop and the threads or processes used"""
        enumerate_threads(is_main=True)
        if self.running.is_set():
            self.running.clear()
            time.sleep(.1)
            self.camera.stop()
            time.sleep(.1)
            self.serial.stop()
            time.sleep(.1)
            self.socketio.stop()
            time.sleep(.1)
            if self.manager is not None:
                self.manager.shutdown()

    def obtain_dict(self):
        """It returns multiprocess usable dict, based on Manager from multiprocessing.
        Note: if all the program runs in one core, it will return a normal dictionary.
        """
        if self.manager is not None:
            return self.manager.dict()
        return dict()

    def backup(self, data:dict):
        """It will shelter your data dictionary before you do a change.
        If something happen during writting you will be able to restore data 
        calling restore function.
        """
        if isinstance(data, dict):
            self._backup = dict(data)
            return 
        self._backup = data 
    
    def restore(self, data):
        """It returns last data backup."""
        if self._backup is not None:
            data = self._backup
            return data