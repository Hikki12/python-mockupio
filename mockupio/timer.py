
from threading import Thread, Event
import time 
from .events import Event as EventEmitter


class SetInterval:
    """Set Interval class based on setInterval from node
    It uses a thread to execute periodically and a callback
    """
    def __init__(self, interval, callback, *args, **kwargs):
        self.interval = interval
        self.callback = callback
        self.event = Event()
        self.args = args 
        self.kwargs = kwargs
        if 'args' in kwargs:
            self.args = kwargs['args']
        
        self.thread = Thread(target=self.run)
        self.thread.start()

    def run(self):
        if self.callback is None:
            raise RuntimeError('Not valid callback!')
        if self.interval < 0:
            raise RuntimeError('Interval (secs) must be a possitive number!')

        while  not self.event.wait(self.interval):
            if self.callback.__code__.co_argcount > 0:
                self.callback(*self.args)
            else:
                self.callback()

    def join(self):
        self.thread.join()

    def cancel(self):
        self.event.set()


class SimpleTimer:
    """Simple timer 
    Args:
        interval: interval in seconds
    """
    def __init__(self, interval=1, *args, **kwargss):
        super().__init__()
        self._interval = interval
        self._start = time.time()
    
    def isReady(self):
        return time.time() - self._start >= self._interval
    
    def reset(self):
        self._start = time.time()


class CallbackTimer(SimpleTimer, EventEmitter):
    """After of specific interval it runs a callaback
    Args:
        interval: interval in seconds 
        callback: callback function
        running: it enables or disables callback execution 
    
    Usage Example:
        timer = CallbackTimer(interval=1, callback=lambda:print("Timer says Hello!"))
        while True:
            timer.run()
    """
    def __init__(self, callback=None, running=True, *args, **kwargs):
        SimpleTimer.__init__(self, *args, **kwargs)
        EventEmitter.__init__(self, *args, **kwargs)
        self.running = running
        self.args = args
        self.kwargs = kwargs
        self.on('isReady', callback)

    def run(self):
        if self.isReady() and self.running:
            args = None
            if 'args' in self.kwargs:
                args = self.kwargs['args']
            
            if args is not None:
                self.emit('isReady', *args)
            else:
                self.emit('isReady')
            self.reset()
    
    def pause(self):
        self.running = False
    
    def resume(self):
        self.running = True
        self.reset()

# def pause_timer(values):
#     print("Pausing...", values)
#     #timer.pause()
# n = 0
# timer = CallbackTimer(interval=1, callback=pause_timer, args=(True,))
# t = CallbackTimer()
# while True:
#     # if timer.isReady():
#     #     print("I'm ready")
#     #     timer.reset()
#     print("Aquí", n)
#     timer.run()
#     time.sleep(0.25)