import socketio
import asyncio
from threading import Thread, Event, Lock
from multiprocessing import Process
import logging
import time 
import os 
import signal
from .asyncio_handler import ignore_aiohttp_ssl_error, unexpected_handler
from .timer import SimpleTimer
from .threads_utils import enumerate_threads


FORMAT = '%(asctime)-10s %(name)-10s %(levelname)-8s %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)


# Connection Routes
identify_mockup = "identify mockup"
identify_web = "identify web"
identify_ok_mockup = "identify ok mockup"
identify_ok_web = "identify ok web"

# Streaming control
stream_control_web_server_mockup = "stream control"
stream_video_mockup_server = "stream video mockup server"
stream_video_server_web = "stream video server web"

# /*Responses updates routes*/
response_updates_web_server = "response web server"
response_updates_server_mockup = "response server mockup"
response_updates_mockup_server = "response mockup server"
response_updates_server_web = "response server web"
response_ok_mockup_server = "resonse ok mockup server"
response_ok_server_web = "response ok server web"
response_ok_web_server = "response ok web server"
response_ok_server_mockup = "response ok server mockup"

# /*Request for updates*/
request_updates_mockup_server = "request mockup server"
request_updates_server_web = "request server web"
request_updates_web_server = "request web mockup"
request_updates_server_mockup = "request server mockup"


# /*Request for quiz*/
quiz_mockup_server = "quiz mockup server"
quiz_server_web = "quiz server web"


# /*Stop routes*/
stop_web_server = "stop web server"
stop_server_mockup = "stop server mockup"
stop_mockup_server = "stop mockup server"
stop_server_web = "stop server web"

# /*Routes for error*/
error_mockup_server = "error mockup server" 


class SocketIO:
    """SocketIO
    :param identifier: (str) an id
    :param server_address: (str)
    :param reconnection_delay: (float, int) seconds
    :param manager: multiprocessing manager object
    :param multiprocess: (bool) if it's True it runs SocketIO in another process
    :param stream_to_host: (bool) if it's True it streams directly to the host without use namespaces
    :param auto_stream: (bool) if it's True it will manage video stream automatically 
    :param write_delay: (float, int) time delay before write data, used for reduce cpu processing
    :param latency: (float) time latency in seconds
    """
    def __init__(self, identifier="" , server_address="", reconnection_delay=2, fps=10, manager=None,
                 multiprocess=True, debug=True, auto_stream=False, stream_to_host=False, latency=0.005,
                 max_stream_time=1800, write_delay=0.25, engine_logger=False, socketio_logger=False,
                **kwargs):
        self.identifier = identifier
        self.server_address = server_address
        self.reconnection_delay = reconnection_delay
        self.streaming_delay = 1/fps - latency
        self.write_delay = write_delay

        self.io = None
        self._configure_socket()

        self.multiprocess = multiprocess
        if manager is not None and multiprocess:
            self.process = Process(target=self.run, name="SocketIO-Process", daemon=True)
            self.running = manager.Event()
            self.connection_event = manager.Event()
            self.stop_event = manager.Event()
            self.incoming_data = manager.Event()
            self.write_event = manager.Event()
            self.busy_event = manager.Event()
            self.image_lock = manager.Lock()
            self.vars = manager.dict()
        else:
            self.process = Thread(target=self.run, name="SocketIO-Thread", daemon=True)
            self.running = Event()
            self.connection_event = Event()
            self.stop_event = Event()
            self.write_event = Event()
            self.incoming_data = Event()
            self.busy_event = Event()
            self.image_lock = Lock()
            self.vars = {}

        self.video_streaming_event = asyncio.Event()
        self.write_event_async = asyncio.Event()
        self.stream_lock = asyncio.Lock()

        self.vars["frame64"] = ""
        self.vars["connected"] = False
        self.vars["pid"] = os.getpid()

        self.stream_to_host = stream_to_host
        self.auto_stream = auto_stream
        self.vars["stream_is_enable"] = not auto_stream

        if auto_stream and max_stream_time != 0:
            self.streamTimer = SimpleTimer(interval=max_stream_time)
        else:
            self.streamTimer = None 

        self.stream_address = stream_video_mockup_server

        if self.stream_to_host:
            self.stream_address = self.server_address  

        if self.auto_stream:
            self.busy_event.set()
            
        self.logger = logging.getLogger('SocketIO')
        self.logger.setLevel(logging.DEBUG)
        self.logger.disabled = not debug

        if not socketio_logger:
            self.logger.debug("Disabling socket-io logger")
            logging.getLogger('socketio').setLevel(logging.ERROR)

        if not engine_logger:
            self.logger.debug("Disabling engine-io logger")
            logging.getLogger('engineio').setLevel(logging.ERROR)

    def _configure_socket(self):
        self.io = socketio.AsyncClient(reconnection=False, logger=True, ssl_verify=False)        
        self.io.on('connect', self.handle_connection)
        self.io.on('disconnect', self.handle_disconnection)
        self.io.on(identify_ok_mockup, self.handle_identify_ok)
        self.io.on(stop_server_mockup, self.handle_stop_event)
        self.io.on(response_updates_server_mockup, self.handle_incoming_data)
        self.io.on(stream_control_web_server_mockup, self.handle_streaming_control)

    def is_busy(self):
        """It returns busy (streaming) state."""
        return bool(self.vars["stream_is_enable"])

    async def handle_streaming_control(self, stream_is_enable:bool):
        """It enables or disables streaming event."""
        self.logger.debug(f"Are you streaming?:  {stream_is_enable}")
        if self.auto_stream:
            self.vars["stream_is_enable"] = stream_is_enable
            if stream_is_enable:
                self.logger.debug("continue with streaming...")
                self.write_event_async.set()
                self.video_streaming_event.set()
            else:
                self.write_event_async.clear()
                self.video_streaming_event.clear()
            self.busy_event.set()
            
            if self.streamTimer is not None:
                self.streamTimer.reset()

    async def handle_incoming_data(self, data):
        """It recives data coming from the server."""
        try: 
            self.vars["data"] = data
            self.incoming_data.set()
        except Exception as e:
            self.logger.debug(e)

    async def handle_stop_event(self):
        """It's called when an stop event is emitted by the server."""
        self.stop_event.set()
            
    async def handle_identify_ok(self):
        self.logger.debug("Identificado correctamente")

    async def handle_connection(self):
        """It is called when a connection ocurrs."""
        self.vars["connected"] = True
        self.connection_event.set()
        self.write_event_async.clear()
        self.logger.debug("CONNECTED...")
        async with self.stream_lock:
            await self.io.emit(identify_mockup, self.identifier)
    
    async def handle_disconnection(self):
        """It is called when a disconnection ocurrs."""
        self.vars["connected"] = False
        self.connection_event.set()
        self.video_streaming_event.clear()
        self.write_event_async.set()
        self.logger.debug("DISCONNECTED...")

    async def streaming(self):
        """It streams video image."""
        while self.running.is_set():
            if self.io.connected and self.vars["stream_is_enable"]:
                
                self.image_lock.acquire()
                frame = self.get_frame()
                self.image_lock.release()

                if frame is not None:
                    try:
                        async with self.stream_lock:
                            await self.io.emit(self.stream_address, frame) 
                        self.clear_frame()
                    except Exception as e:
                        self.logger.debug(e)

                if self.streamTimer is not None:
                    if self.streamTimer.isReady():
                        await self.handle_streaming_control(False)
                        self.streamTimer.reset()
            else:
                self.logger.debug("waiting for streaming...")
                await self.video_streaming_event.wait()
            await asyncio.sleep(self.streaming_delay)

    async def connect(self):
        """It tries to connect with the server."""
        try:
            await self.io.disconnect()
            await self.io.connect(self.server_address, transports='websocket')
        except Exception as e:
            self.logger.debug(e)

    async def auto_connect(self):
        """It tries to connect with the server, if socket disconnects it will reconnect it."""
        
        while self.running.is_set():
            if self.io.connected:
                await self.manage_write_event()
                if not self.is_busy():
                    await self.write_event_async.wait()
                await asyncio.sleep(self.write_delay)
            else:
                await self.connect()
                await asyncio.sleep(self.reconnection_delay)
            
        self.logger.debug("Ending auto-connect...")

    async def manage_write_event(self):
        """It will manage the write event with the socketio"""
        if self.write_event.is_set():
            await self.send(self.vars["msg"])
            self.write_event.clear()

    async def send(self, msg):
        """It emits a message to the socketio server."""
        try:
            async with self.stream_lock:
                await self.io.emit(response_updates_mockup_server, msg)
        except Exception as e:
            self.logger.debug(e)

    async def create_tasks(self):
        """It creates async tasks."""
        await asyncio.gather(self.auto_connect(), self.streaming(), )

    def is_connected(self):
        """It returns connection status."""
        return bool(self.vars["connected"])

    def set_new_frame(self, frame64:str):
        """It passes a new frame on base 64 to this client.
        Note: this function could be used for different processes.
        """
        self.image_lock.acquire()
        self.vars["frame64"] = frame64
        self.image_lock.release()

    def get_frame(self):
        """It returns the current frame on base 64."""
        if len(self.vars["frame64"]) > 10:
            return self.vars["frame64"] 
        return None
    
    def clear_frame(self):
        """It clear the last frame on base 64."""
        self.image_lock.acquire()
        self.vars["frame64"] = ""
        self.image_lock.release()

    def write(self, msg):
        """It set the write event.
        Note: this function could be used for different processes.
        """
        self.vars["msg"] = msg
        self.write_event.set()

    def get_data(self):
        """It returns the last data recived from the server"""
        return self.vars["data"]
        
    def run(self):
        """It configures the event loop and its tasks."""
        self.vars["pid"] = os.getpid()
        self._configure_socket()
        asyncio.set_event_loop(asyncio.new_event_loop())
        self.video_streaming_event = asyncio.Event()
        self.write_event_async = asyncio.Event()
        self.loop = asyncio.get_event_loop()
        ignore_aiohttp_ssl_error(self.loop)
        unexpected_handler(self.loop)
        self.loop.run_until_complete(self.create_tasks())

    def start(self): 
        """It starts a new thread or a new procces as appropiate."""
        self.running.set()
        self.process.start()
        enumerate_threads(self.process)
    
    async def end(self):
        """It clears running event."""
        self.video_streaming_event.set()
        self.write_event_async.set()        
        await asyncio.sleep(.1)
        self.running.clear()

    def stop(self):
        """It will stop the thread or the process as appropiate.
        Note: this function could be used by different processes.
        """
        enumerate_threads(self.process)
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.end())
        self.logger.debug("Ending SocketIO...")
        if self.multiprocess:
            os.kill(self.vars["pid"], signal.SIGTERM)

