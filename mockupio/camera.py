from multiprocessing import process
from typing import Callable
import threading
import cv2
from concurrent.futures import ThreadPoolExecutor
import threading
import queue
import multiprocessing
from .camera_utils import *
import logging
import time
from .threads_utils import enumerate_threads


FORMAT = '%(asctime)-10s %(name)-10s %(levelname)-8s %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)


class Camera:
    """Custom camera class.
    :param i: (int) index of the camera
    :param size: (tuple) image size
    :param h_flip: (bool) flip horizontally?
    :param v_flip: (bool) flip vertically?
    :param qualilty: (float, int) quality of jpeg image
    :param manager: multiprocessing manager object
    :param multiprocess: (bool) if it's True it runs camera in another process
    :param debug: (bool) it enables the logger
    :param dt_fix: (float, int) it fix the delay for obtain the desired fps speed
-    """
    def __init__(self, i=0, fps=10, size=(320, 240), h_flip=True, v_flip=False, quality=90, manager=None,
                 multiprocess=True, debug=True, dt_fix=0.01, power_save=False, rotate=0):
        self.i = i
        self.video = None
        self.h_flip = h_flip
        self.v_flip = v_flip
        self.size = size
        self.fps = fps
        self.interval = 1/self.fps - dt_fix
        self.quality = quality

        #
        self.preprocess = None
        self.preprocess_kwargs = None

        #
        self.after_read = None
        self.after_read_kwargs = None

        self.after_record = None
        self.after_read_kwargs = None

        self.available_callaback = ["preprocess", "after-read", "after-record"]

        if manager is not None and multiprocess:
            self.data = multiprocessing.Queue()
            self.process = multiprocessing.Process(
                target=self.run, name="Camera-Process", daemon=True)
            self.running = manager.Event()
            self.wait_event = manager.Event()
            self.record_end = manager.Event()
        else:
            self.data = queue.Queue()
            self.process = threading.Thread(
                target=self.run, name="Camera-Thread", daemon=True)
            self.running = threading.Event()
            self.wait_event = threading.Event()
            self.record_end = threading.Event()
        
        self.power_save = power_save
        if not self.power_save:
            self.active_cpu()

        self.logger = logging.getLogger('Camera')
        self.logger.setLevel(logging.DEBUG)
        self.logger.disabled = not debug

    def start(self):
        """It start the process or the thread as appropiate."""
        self.process.start()
        enumerate_threads(self.process)

    def is_power_save(self):
        return self.power_save

    def relax_cpu(self):
        """Enable wait event to relax CPU"""
        if self.power_save:
            self.logger.debug("Relaxing CPU")
        self.wait_event.clear()

    def active_cpu(self):
        """Resume cpu activity"""
        if self.power_save:
            self.logger.debug("Activing CPU")
        self.wait_event.set()

    def run(self):
        """It will run a loop for the camera"""
        self.video = cv2.VideoCapture(self.i)
        self.executor = ThreadPoolExecutor(max_workers=1)
        self.running.set()
        while self.running.is_set():
            self.read_frame()
            if self.power_save:
                self.wait_event.wait()
            time.sleep(self.interval)
        self.logger.debug("Ending camera...")

    def read_frame(self):
        """Read frame from camera"""
        # Read from camera
        ret, img = self.video.read()
        if ret:
            img = resize_image(img, self.size)
            img = flip_image(img, self.h_flip, self.v_flip)

            if self.preprocess is not None:
                if self.preprocess_kwargs is not None:
                    img = self.preprocess(img, **self.preprocess_kwargs)
                else:
                    img = self.preprocess(img)

            if self.after_read is not None:
                if self.after_read_kwargs is not None:
                    returned = self.after_read(img, **self.after_read_kwargs)
                else:
                    returned = self.after_read(img)
                img64 = encode_base64(img, self.quality)
                self.data.put([img, img64, returned])
            else:
                img64 = encode_base64(img, self.quality)
                self.data.put([img, img64, None])

        self.record_video(img)

    def record_video(self, frame):
        pass

    def stop(self):
        """It stops camera process or thread."""
        self.running.set()
        enumerate_threads(self.process)
        
    def read(self):
        """It returns a data tuple
        data is a tuple with the image frame obtained from camera, the frame encoded to jpeg and base64,
        and the results of some processing function. 
        Note: this function could be used by different processes.
        
        Example:
            frame, frame64, resutls = self.data.get(True)
        """
        return tuple(self.data.get(True))

    def set_callback(self, when: str, callback: Callable, **kwargs):
        """It sets a multiprocessing callback"""
        if when == 'preprocess':
            self.preprocess = callback
            self.preprocess_kwargs = kwargs
            return

        if when == 'after-read':
            self.after_read = callback
            self.after_read_kwargs = kwargs
            return

        if when == 'after-record':
            self.after_record = callback
            self.after_record_kwargs = kwargs
            return
        raise RuntimeError('Not valid callback, you should use: {0} or {1} or {2}'.format(*self.available_callaback))
