import multiprocessing
import threading 
from multiprocessing import Process


def enumerate_threads(process=None, is_main=False):
    """It will list threads on a process"""
    if isinstance(process, Process):
        print(f"Threads on {process.name} process: ")
        for thread in threading.enumerate(): 
            print("  - ", thread.name, ", is alive: ", thread.is_alive())
    else:
        if is_main:
            print("Threads on Main process: ")
            for thread in threading.enumerate(): 
                print("  - ", thread.name, ", is alive: ", thread.is_alive())