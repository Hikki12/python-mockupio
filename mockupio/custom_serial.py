import serial
from threading import Thread, Event
from multiprocessing import Process
import serial.tools.list_ports
import time
import json
import logging
from .events import Event as EventEmitter
from .threads_utils import enumerate_threads


FORMAT = '%(asctime)-10s %(name)-10s %(levelname)-8s %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)


class Serial:
    """A custom serial class
    
    :param manager: multiprocessing manager object
    :param port: (str) port name
    :param baudrate: (int, str) baudrate
    :param timeout: (int, float) read timeout
    :param reconnection_delay: (int, float) seconds
    :param multiprocess: (bool) if it's True it runs Serial in another process
    """
    def __init__(self, manager=None, port=None, baudrate=9600, timeout=0.25, reconnection_delay=1,
                 multiprocess=True, debug=True):
        self.serialPort = serial.Serial()
        self.port = port
        self.serialPort.timeout = timeout
        self.serialPort.baudrate = baudrate
        self.read_errors = 0
        self.max_read_errors = 10
        self.reconnection_delay = reconnection_delay

        self.reconnection_attempts = 0
        self.max_reconnection_attempts = 10
        self.ports_list = []

        self.events = EventEmitter()

        if manager is not None and multiprocess:
            self.process = Process(target=self.run, name="Serial-Process", daemon=True)
            self.running = manager.Event()
            self.connection_event = manager.Event()
            self.write_event = manager.Event()
            self.incoming_data = manager.Event()
            self.vars = manager.dict()
        else:
            self.process = Thread(target=self.run, name="Serial-Thread", daemon=True)
            self.running = Event()
            self.connection_event = Event()
            self.write_event = Event()
            self.incoming_data = Event()
            self.vars = dict()

        self.vars["connected"] = False

        #
        self.logger = logging.getLogger('Serial')
        self.logger.setLevel(logging.DEBUG)
        self.logger.disabled = not debug

    def on(self, event_name:str, callback):
        """It sets an event and a callback function"""
        self.events.on(event_name, callback)

    def update_ports_list(self):
        """It updates the internal list of serial port devices"""
        self.ports_list = [
            port.device for port in serial.tools.list_ports.comports()]

    def print_ports_list(self, updateFirst:bool=True):
        """It prints on console the serial port devices
        :param updateFirst: ``True`` if you prefer to update the list of serial devices and after print data
        """
        if updateFirst:
            self.update_ports_list
        print("SERIAL DEVICES")
        for i in range(len(self.ports_list)):
            print(f"   Device {i}: {self.ports_list[i]}")

    def run(self):
        """It will run de loop for serial"""
        while self.running.is_set():
            self.vars["connected"] = self.serialPort.is_open
            if not self.serialPort.is_open:
                self.auto_connect()
                time.sleep(self.reconnection_delay)
            else:
                self.readlines()
                self.manage_write_event()

    def readlines(self):
        """It will read a message"""
        try:
            data = self.serialPort.readline().decode().rstrip()
            if len(data) > 0:
                self.vars["data"] = data
                self.incoming_data.set()
                self.events.emit('data-incoming', data)
            self.serialPort.flush()

        except Exception as e:
            self.read_errors += 1
            self.logger.debug(e)
            if self.read_errors >= self.max_read_errors:
                self.disconnect()
                time.sleep(self.reconnection_delay)
                self.read_errors = 0

    def get_data(self):
        return self.vars["data"]

    def auto_connect(self):
        """It will autoreconnect with the serial device if something happens"""
        if self.port is not None:
            self.serialPort.port = self.port
        else:
            self.update_ports_list()
            if len(self.ports_list) > 0:
                self.serialPort.port = self.ports_list[0]

        try:
            self.serialPort.open()
        except:
            self.reconnection_attempts += 1
            if self.reconnection_attempts >= self.max_reconnection_attempts:
                self.reconnection_attempts = 0
                self.logger.debug("Something HAPPENS WITH ARDUINO")

        if self.serialPort.is_open:
            self.read_errors = 0
            self.reconnection_attempts = 0
            self.connection_event.set()
            self.serialPort.reset_input_buffer()
            self.serialPort.reset_output_buffer()
            self.serialPort.flush()
        else:
            self.connection_event.clear()

    def start(self):
        """It will start serial process or thread."""
        self.running.set()
        self.process.start()
        enumerate_threads(self.process)

    def disconnect(self):
        """It closes the serial port."""
        if self.serialPort.is_open:
            self.serialPort.close()
            self.connection_event.set()

    def send(self, data:str):
        """Send data message to the serial device."""
        if self.serialPort.is_open:
            try:
                message = str(data) + "\n"
                self.serialPort.write(message.encode())
            except Exception as e:
                self.logger.debug(e)

    def manage_write_event(self):
        """When a message required to be writted it will send the message to the serial device."""
        if self.write_event.is_set():
            self.send(self.vars["message"])
            self.write_event.clear()

    def write(self, message="", toJson:bool=True):
        """It will write a msg to serial device"""
        if toJson:
            try:
                message = json.dumps(message)
            except Exception as e:
                self.logger(e)
        self.vars["message"] = message
        self.write_event.set()

    def is_connected(self):
        """It will return the current state of the serial port connection."""
        return not bool(self.vars["connected"])

    def stop(self):
        """It will stop serial process or thread as appropiate."""
        enumerate_threads(self.process)
        if self.running.set():
            self.running.clear()
        if self.serialPort.is_open:
            self.serialPort.close()
        self.logger.debug("Ending Serial...")


# def read(data):
#   print("data: ", data)

# vars = {
#   "var1": False,
#   "var2": 100,
#   "var3": 4.555,
#   "var4": 5001
# }

# s = Serial()
# s.on('data-incoming', read)
# s.start()
# time.sleep(3)
# s.write(vars)
# time.sleep(2)
# s.write("$stop", toJson=False)
