import cv2
import numpy as np
import base64


def resize_image(img, size=(320, 240)):
    """It resizes an image array
    :param img: image array
    :param size: size tuple
    """
    return cv2.resize(img, size)


def flip_image(img, h_flip=False, v_flip=False):
    """It flips an image array vertically or horizontally
    :param img: image array
    :param h_flip: horizontal flip flag
    :param v_flip: vertical flip flag
    """
    if h_flip:
        img = cv2.flip(img, 1)
    if v_flip:
        img = cv2.flip(img, 0)
    return img


def encode_base64(img=None, quality=90):
    """It encodes image to base64 to send it through socketio to nodejs server
    :param img: image array
    :param quality: 0-100 value of quality
    :return encoded: an image in base64 format
    """
    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), quality]
    _, img = cv2.imencode('.jpg', img, encode_param)
    encoded = base64.b64encode(img.tobytes()).decode()
    return encoded


def image_adjust(img, a=0.8, b=0.8, sat=1.1):
    """It adjust contrast, brightness and saturation of a image
    :param img: image array
    :param a: contrast
    :param b: brightness
    :param sat: saturation
    :return: img: image array processed
    """
    img = cv2.convertScaleAbs(img, alpha=a, beta=b)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV).astype("float32")
    h, s, v = cv2.split(hsv)
    s = s * sat
    s = np.clip(s, 0, 255)
    hsv = cv2.merge([h, s, v])
    img = cv2.cvtColor(hsv.astype("uint8"), cv2.COLOR_HSV2BGR)
    return img


def crop_image(img, xratio=0.7, yratio=1):
    center = np.array(img.shape)/2
    x = int(center[1])
    y = int(center[0])
    a = x - int(xratio*x)
    b = x + int(xratio*x)
    c = y - int(yratio*y)
    d = y + int(yratio*y)
    new = img[c:d, a:b]
    return new
