from setuptools import setup

setup(
    name="mockupio",  # Nombre
    version="0.1",  # Version de desarrollo
    description="Library for build remote laboratory experiments",  # Descripcion del funcionamiento
    author="Jason Francisco Macas Mora",  # Nombre del autor
    author_email='franciscomacas3@gmail.com',  # Email del autor
    license="MIT",  # Licencia: MIT, GPL, GPL 2.0...
    url="https://Hikki12@bitbucket.org/Hikki12/python-mockupio.git",  # Página oficial (si la hay)
    packages=['mockupio'],
    install_requires=[i.strip() for i in open("./requirements.txt").readlines()]
)