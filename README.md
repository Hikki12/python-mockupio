# python-mockupio

**MockupIO** is a library that provides a set of tools for make mockups for **Remote Labs** (RL).
It is based on communication protocols such as **socket-io** and **serial**. It allows to record and stream video over a network using **opencv**.

Note: It only works on **Linux**.

# Virtual env
It is recommended to create a virtual environment.
The virtualenv package is required to create virtual environments. You can install it with pip:
```
pip3 install virtualenv
```
Then you can create your enviroment as follows:
```
virtualenv my-venv-name
```
After this, you should active it:
```
source my-venv-name/bin/activate
```
# Install

Clone this repository and then run this:
```
sudo chmod u+x install.sh
./install.sh
```

# Creating a new mockup
You can check examples to see a template.
# Uninstall
```To remove
sudo chmod u+x uninstall.sh
./uninstall.sh
```
# License
Allright reserved 2021 ©